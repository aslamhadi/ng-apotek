import { NgApotekPage } from './app.po';

describe('ng-apotek App', () => {
  let page: NgApotekPage;

  beforeEach(() => {
    page = new NgApotekPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
