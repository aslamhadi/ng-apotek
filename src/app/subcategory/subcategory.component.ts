import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { SubCategory, SubCategoryService } from '../shared';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DeleteComponentDialog } from '../shared';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'app-subCategory',
  templateUrl: './subcategory.component.html',
})
export class SubCategoryComponent {
  displayedColumns = [
    'name',
    'category',
    'idx_sale_price',
    'idx_sale_price_prescription',
    'action',
  ];
  dataSource: SubCategoryDataSource | null;
  dataSubject = new BehaviorSubject<any[]>([]);
  errorMessage: string;
  loading: boolean = false;

  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private subCategoryService: SubCategoryService,
    public dialog: MatDialog
  ) {}

  getCategories(): void {
    this.loading = true;
    this.subCategoryService.getAll().subscribe(categories => {
      this.dataSubject.next(categories);
      this.loading = false;
    }, error => (this.errorMessage = <any>error));

    this.dataSource = new SubCategoryDataSource(
      this.dataSubject,
      this.paginator
    );
  }

  ngOnInit() {
    this.getCategories();
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  openDialog(subCategory) {
    let data = {
      name: subCategory.name,
      title: 'Subkategori',
    };
    let dialogRef = this.dialog.open(DeleteComponentDialog, {
      data: data,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteSubCategory(subCategory.id);
      }
    });
  }

  deleteSubCategory(subCategoryId) {
    this.loading = true;
    this.subCategoryService.delete(subCategoryId).subscribe(
      data => this.getCategories(),
      err => {
        alert(err);
      }
    );
  }
}

export class SubCategoryDataSource extends DataSource<any[]> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: SubCategory[] = [];

  constructor(
    private subject: BehaviorSubject<any[]>,
    private _paginator: MatPaginator
  ) {
    super();
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.subject,
      this._paginator.page,
      this._filterChange,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      // filter data by search term
      this.filteredData = this.subject.value
        .slice()
        .filter((item: SubCategory) => {
          let searchStr = item.name.toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) != -1;
        });

      // copy data so it doesn't get mutated
      const data: SubCategory[] = this.filteredData.slice();

      // Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect(): void {}
}
