import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import {
  SubCategory,
  Category,
  Errors,
  CategoryService,
  SubCategoryService,
} from '../shared';

@Component({
  selector: 'edit-subcategory',
  templateUrl: './subcategory.add.component.html',
})
export class SubCategoryEditComponent implements OnInit {
  categories: Category[] = [];
  errorMessage: string;
  subCategoryForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  subCategory: SubCategory;
  subCategoryId: string = '';
  pageTitle: string = 'Ubah Subkategori';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService,
    private subCategoryService: SubCategoryService,
    private fb: FormBuilder
  ) {
    this.subCategoryForm = this.fb.group({
      name: [null, Validators.required],
      categoryId: [null, Validators.required],
    });
  }

  ngOnInit() {
    this.getCategory();
    this.getCategories();
  }

  getCategories(): void {
    this.categoryService.getAll().subscribe(categories => {
      this.categories = categories;
    }, error => (this.errorMessage = <any>error));
  }

  getCategory() {
    this.route.params
      .switchMap(
        (params: Params) => (
          (this.subCategoryId = params['id']),
          this.subCategoryService.get(params['id'])
        )
      )
      .subscribe((subCategory: SubCategory) => {
        this.subCategory = subCategory;
        this.subCategoryForm.patchValue({
          name: subCategory.name,
          categoryId: subCategory.category.id,
        });
      });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let subCategory = this.subCategoryForm.value;
    this.subCategoryService.update(subCategory, this.subCategoryId).subscribe(
      data => this.router.navigateByUrl('/subcategories'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
