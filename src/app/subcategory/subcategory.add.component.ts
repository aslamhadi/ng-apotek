import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import {
  SubCategory,
  Category,
  Errors,
  CategoryService,
  SubCategoryService,
} from '../shared';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.add.component.html',
})
export class SubCategoryAddComponent {
  categories: Category[] = [];
  errorMessage: string;
  subCategoryForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  pageTitle: string = 'Tambah Subkategori';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService,
    private subCategoryService: SubCategoryService,
    private fb: FormBuilder
  ) {
    this.subCategoryForm = this.fb.group({
      name: [null, Validators.required],
      categoryId: [null, Validators.required],
    });
  }

  getCategories(): void {
    this.categoryService.getAll().subscribe(categories => {
      this.categories = categories;
    }, error => (this.errorMessage = <any>error));
  }

  ngOnInit() {
    this.getCategories();
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let subCategory = this.subCategoryForm.value;
    this.subCategoryService.add(subCategory).subscribe(
      data => this.router.navigateByUrl('/subcategories'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
