import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  loggedIn: boolean = false;
  listLinks = [];

  constructor(private router: Router, private userService: UserService) {}

  ngOnInit() {
    this.userService.populate();

    this.userService.isAuthenticated.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.listLinks = [
          { link: '/categories', name: 'Kategori' },
          { link: '/subcategories', name: 'Subkategori' },
          { link: '/unit-types', name: 'Satuan' },
          { link: '/doctors', name: 'Dokter' },
          { link: '/embalases', name: 'Embalase' },
        ];
      } else {
        this.listLinks = [{ link: '/login', name: 'Login' }];
      }
    });
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/login');
  }
}
