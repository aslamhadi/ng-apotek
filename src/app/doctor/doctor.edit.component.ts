import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { Doctor, Errors, DoctorService } from '../shared';

@Component({
  selector: 'edit-doctor',
  templateUrl: './doctor.add.component.html',
})
export class DoctorEditComponent implements OnInit {
  doctors: Doctor[] = [];
  errorMessage: string;
  doctorForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  doctor: Doctor;
  doctorId: string = '';
  pageTitle: string = 'Ubah Dokter';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private doctorService: DoctorService,
    private fb: FormBuilder
  ) {
    this.doctorForm = this.fb.group({
      first_name: [null, Validators.required],
      email: '',
      last_name: '',
      desc: '',
      address: '',
      city: '',
      phone_number: '',
    });
  }

  ngOnInit() {
    this.getDoctor();
  }

  getDoctor() {
    this.route.params
      .switchMap(
        (params: Params) => (
          (this.doctorId = params['id']),
          this.doctorService.get(params['id'])
        )
      )
      .subscribe((doctor: Doctor) => {
        this.doctor = doctor;
        this.doctorForm.patchValue({
          first_name: doctor.user.first_name,
          last_name: doctor.user.last_name,
          email: doctor.user.email,
          desc: doctor.desc,
          city: doctor.city,
          phone_number: doctor.phone_number,
        });
      });
    console.log(this.doctorForm);
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let rawDoctor = this.doctorForm.value;
    let user = {
      email: rawDoctor.email,
      first_name: rawDoctor.first_name,
      last_name: rawDoctor.last_name,
    };
    let doctor = {
      user: user,
      address: rawDoctor.address,
      desc: rawDoctor.desc,
      city: rawDoctor.city,
      phone_number: rawDoctor.phone_number,
    };
    this.doctorService.update(doctor, this.doctorId).subscribe(
      data => this.router.navigateByUrl('/doctors'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
