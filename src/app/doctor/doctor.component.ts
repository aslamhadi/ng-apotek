import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Doctor, DoctorService } from '../shared';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DeleteComponentDialog } from '../shared';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
})
export class DoctorComponent {
  displayedColumns = [
    'name',
    'email',
    'desc',
    'phone_number',
    'address',
    'action',
  ];
  dataSource: DoctorDataSource | null;
  dataSubject = new BehaviorSubject<any[]>([]);
  errorMessage: string;
  loading: boolean = false;

  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private doctorService: DoctorService,
    public dialog: MatDialog
  ) {}

  getDoctors(): void {
    this.loading = true;
    this.doctorService.getAll().subscribe(doctors => {
      this.dataSubject.next(doctors);
      this.loading = false;
    }, error => (this.errorMessage = <any>error));

    this.dataSource = new DoctorDataSource(this.dataSubject, this.paginator);
  }

  ngOnInit() {
    this.getDoctors();
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  openDialog(doctor) {
    let data = {
      name: doctor.name,
      title: 'Dokter',
    };
    let dialogRef = this.dialog.open(DeleteComponentDialog, {
      data: data,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteDoctor(doctor.id);
      }
    });
  }

  deleteDoctor(doctorId) {
    this.loading = true;
    this.doctorService.delete(doctorId).subscribe(
      data => this.getDoctors(),
      err => {
        alert(err);
      }
    );
  }
}

export class DoctorDataSource extends DataSource<any[]> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Doctor[] = [];

  constructor(
    private subject: BehaviorSubject<any[]>,
    private _paginator: MatPaginator
  ) {
    super();
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.subject,
      this._paginator.page,
      this._filterChange,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      // filter data by search term
      this.filteredData = this.subject.value.slice().filter((item: Doctor) => {
        let searchStr = item.user.full_name.toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });

      // copy data so it doesn't get mutated
      const data: Doctor[] = this.filteredData.slice();

      // Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect(): void {}
}
