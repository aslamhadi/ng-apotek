import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { Category, Errors, CategoryService } from '../shared';

@Component({
  selector: 'app-category',
  templateUrl: './category.add.component.html',
})
export class CategoryAddComponent {
  categories: Category[] = [];
  errorMessage: string;
  categoryForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  pageTitle: string = 'Tambah Kategori';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService,
    private fb: FormBuilder
  ) {
    this.categoryForm = this.fb.group({
      name: [null, Validators.required],
      idx_sale_price: [null, Validators.required],
      idx_sale_price_prescription: [null, Validators.required],
    });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let category = this.categoryForm.value;
    this.categoryService.add(category).subscribe(
      data => this.router.navigateByUrl('/categories'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
