import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { Category, Errors, CategoryService } from '../shared';

@Component({
  selector: 'edit-category',
  templateUrl: './category.add.component.html',
})
export class CategoryEditComponent implements OnInit {
  categories: Category[] = [];
  errorMessage: string;
  categoryForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  category: Category = new Category('', '', 0, 0);
  categoryId: string = '';
  pageTitle: string = 'Ubah Kategori';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService,
    private fb: FormBuilder
  ) {
    this.categoryForm = this.fb.group({
      name: [null, Validators.required],
      idx_sale_price: [null, Validators.required],
      idx_sale_price_prescription: [null, Validators.required],
    });
  }

  ngOnInit() {
    this.getCategory();
  }

  getCategory() {
    this.route.params
      .switchMap(
        (params: Params) => (
          (this.categoryId = params['id']),
          this.categoryService.get(params['id'])
        )
      )
      .subscribe((category: Category) => {
        this.category = category;
        this.categoryForm.patchValue({
          name: category.name,
          idx_sale_price: category.idx_sale_price,
          idx_sale_price_prescription: category.idx_sale_price_prescription,
        });
      });
    console.log(this.categoryForm);
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let category = this.categoryForm.value;
    this.categoryService.update(category, this.categoryId).subscribe(
      data => this.router.navigateByUrl('/categories'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
