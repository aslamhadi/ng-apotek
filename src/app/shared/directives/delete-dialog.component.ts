import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'delete-category',
  template: `
    <h1 md-dialog-title>Hapus {{data.title}}</h1>
    <div md-dialog-content>Apakah anda yakin akan menghapus {{ data.name }} ? </div>
    <div md-dialog-actions>
      <button md-button mat-dialog-close>Tidak</button>
      <button md-button [mat-dialog-close]="true">Hapus</button>
    </div>  
  `,
})
export class DeleteComponentDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteComponentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
}
