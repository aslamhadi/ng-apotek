import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { Doctor } from '../models';

import Utils from './utils';

@Injectable()
export class DoctorService {
  baseUrl = '/api/doctors/';

  constructor(private apiService: ApiService) {}

  add(payload): Observable<Doctor> {
    return this.apiService
      .post(this.baseUrl, {
        user: {
          email: payload.user.email,
          first_name: payload.user.first_name,
          last_name: payload.user.last_name,
        },
        address: payload.address,
        desc: payload.desc,
        city: payload.city,
        phone_number: payload.phone_number,
      })
      .map(data => data);
  }

  update(payload, doctorId): Observable<Doctor> {
    return this.apiService
      .put(`${this.baseUrl}${doctorId}/`, {
        user: {
          email: payload.user.email,
          first_name: payload.user.first_name,
          last_name: payload.user.last_name,
        },
        address: payload.address,
        desc: payload.desc,
        city: payload.city,
        phone_number: payload.phone_number,
      })
      .map(data => data);
  }

  getAll(): Observable<Doctor[]> {
    return this.apiService.get(this.baseUrl).map(data => data);
  }

  get(doctorId): Observable<Doctor> {
    return this.apiService.get(`${this.baseUrl}${doctorId}/`).map(data => data);
  }

  delete(doctorId) {
    return this.apiService.delete(`${this.baseUrl}${doctorId}`);
  }
}
