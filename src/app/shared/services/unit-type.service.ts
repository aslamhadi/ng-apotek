import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { UnitType } from '../models';

import Utils from './utils';

@Injectable()
export class UnitTypeService {
  baseUrl = '/api/unit-types/';

  constructor(private apiService: ApiService) {}

  add(payload): Observable<UnitType> {
    return this.apiService
      .post(this.baseUrl, {
        name: payload.name,
      })
      .map(data => data);
  }

  update(payload, unitTypeId): Observable<UnitType> {
    return this.apiService
      .put(`${this.baseUrl}${unitTypeId}/`, {
        name: payload.name,
      })
      .map(data => data);
  }

  getAll(): Observable<UnitType[]> {
    return this.apiService.get(this.baseUrl).map(data => data);
  }

  get(unitTypeId): Observable<UnitType> {
    return this.apiService
      .get(`${this.baseUrl}${unitTypeId}/`)
      .map(data => data);
  }

  delete(unitTypeId) {
    return this.apiService.delete(`${this.baseUrl}${unitTypeId}`);
  }
}
