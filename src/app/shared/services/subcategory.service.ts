import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { SubCategory } from '../models';

import Utils from './utils';

@Injectable()
export class SubCategoryService {
  baseUrl = '/api/subcategories/';

  constructor(private apiService: ApiService) {}

  add(payload): Observable<SubCategory> {
    return this.apiService
      .post(this.baseUrl, {
        name: payload.name,
        category_id: payload.categoryId,
      })
      .map(data => data);
  }

  update(payload, subCategoryId): Observable<SubCategory> {
    return this.apiService
      .put(`${this.baseUrl}${subCategoryId}/`, {
        name: payload.name,
        category_id: payload.categoryId,
      })
      .map(data => data);
  }

  getAll(): Observable<SubCategory[]> {
    return this.apiService.get(this.baseUrl).map(data => data);
  }

  get(subCategoryId): Observable<SubCategory> {
    return this.apiService
      .get(`${this.baseUrl}${subCategoryId}/`)
      .map(data => data);
  }

  delete(subCategoryId) {
    return this.apiService.delete(`${this.baseUrl}${subCategoryId}`);
  }
}
