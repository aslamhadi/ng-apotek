import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { Category } from '../models';

import Utils from './utils';

@Injectable()
export class CategoryService {
  baseUrl = '/api/categories/';

  constructor(private apiService: ApiService) {}

  add(payload): Observable<Category> {
    return this.apiService
      .post(this.baseUrl, {
        name: payload.name,
        idx_sale_price: Utils.roundDecimal(payload.idx_sale_price),
        idx_sale_price_prescription: Utils.roundDecimal(
          payload.idx_sale_price_prescription
        ),
      })
      .map(data => data);
  }

  update(payload, categoryId): Observable<Category> {
    return this.apiService
      .put(`${this.baseUrl}${categoryId}/`, {
        name: payload.name,
        idx_sale_price: Utils.roundDecimal(payload.idx_sale_price),
        idx_sale_price_prescription: Utils.roundDecimal(
          payload.idx_sale_price_prescription
        ),
      })
      .map(data => data);
  }

  getAll(): Observable<Category[]> {
    return this.apiService.get(this.baseUrl).map(data => data);
  }

  get(categoryId): Observable<Category> {
    return this.apiService
      .get(`${this.baseUrl}${categoryId}/`)
      .map(data => data);
  }

  delete(categoryId) {
    return this.apiService.delete(`${this.baseUrl}${categoryId}`);
  }
}
