export default class Utils {
  static roundDecimal(value: string) {
    return parseFloat(value).toFixed(2);
  }
}
