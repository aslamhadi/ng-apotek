import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { Embalase } from '../models';

import Utils from './utils';

@Injectable()
export class EmbalaseService {
  baseUrl = '/api/embalases/';

  constructor(private apiService: ApiService) {}

  add(payload): Observable<Embalase> {
    return this.apiService
      .post(this.baseUrl, {
        name: payload.name,
        price: Utils.roundDecimal(payload.price),
      })
      .map(data => data);
  }

  update(payload, embalaseId): Observable<Embalase> {
    return this.apiService
      .put(`${this.baseUrl}${embalaseId}/`, {
        name: payload.name,
        price: Utils.roundDecimal(payload.price),
      })
      .map(data => data);
  }

  getAll(): Observable<Embalase[]> {
    return this.apiService.get(this.baseUrl).map(data => data);
  }

  get(embalaseId): Observable<Embalase> {
    return this.apiService.get(`${this.baseUrl}${embalaseId}/`).map(data => data);
  }

  delete(embalaseId) {
    return this.apiService.delete(`${this.baseUrl}${embalaseId}`);
  }
}
