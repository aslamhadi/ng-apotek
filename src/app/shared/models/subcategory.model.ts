import { Category } from './category.model';

export class SubCategory {
  id: string;
  name: string;
  category: Category;
}
