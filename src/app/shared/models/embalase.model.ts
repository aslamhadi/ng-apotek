export class Embalase {
  id: string;
  name: string;
  price: number;
  created_at: Date;
  modified_at: Date;
}
