export class Category {
  constructor(
    public id: string,
    public name: string,
    public idx_sale_price: number,
    public idx_sale_price_prescription: number
  ) {}
}
