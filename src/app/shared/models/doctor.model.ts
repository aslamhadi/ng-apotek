import { User } from './user.model';

export class Doctor {
  id: string;
  user: User;
  address: string;
  desc: string;
  city: string;
  phone_number: string;
}
