import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { UnitType, Errors, UnitTypeService } from '../shared';

@Component({
  selector: 'edit-unit-type',
  templateUrl: './unit-type.add.component.html',
})
export class UnitTypeEditComponent implements OnInit {
  unitTypes: UnitType[] = [];
  errorMessage: string;
  unitTypeForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  unitType: UnitType = new UnitType('', '');
  unitTypeId: string = '';
  pageTitle: string = 'Ubah Satuan';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private unitTypeService: UnitTypeService,
    private fb: FormBuilder
  ) {
    this.unitTypeForm = this.fb.group({
      name: [null, Validators.required],
    });
  }

  ngOnInit() {
    this.getUnitType();
  }

  getUnitType() {
    this.route.params
      .switchMap(
        (params: Params) => (
          (this.unitTypeId = params['id']),
          this.unitTypeService.get(params['id'])
        )
      )
      .subscribe((unitType: UnitType) => {
        this.unitType = unitType;
        this.unitTypeForm.patchValue({
          name: unitType.name,
        });
      });
    console.log(this.unitTypeForm);
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let unitType = this.unitTypeForm.value;
    this.unitTypeService.update(unitType, this.unitTypeId).subscribe(
      data => this.router.navigateByUrl('/unit-types'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
