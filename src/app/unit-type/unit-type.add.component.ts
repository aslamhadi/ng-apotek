import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { UnitType, Errors, UnitTypeService } from '../shared';

@Component({
  selector: 'app-unit-type',
  templateUrl: './unit-type.add.component.html',
})
export class UnitTypeAddComponent {
  unitTypes: UnitType[] = [];
  errorMessage: string;
  unitTypeForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  pageTitle: string = 'Tambah Satuan';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private unitTypeService: UnitTypeService,
    private fb: FormBuilder
  ) {
    this.unitTypeForm = this.fb.group({
      name: [null, Validators.required],
    });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let unitType = this.unitTypeForm.value;
    this.unitTypeService.add(unitType).subscribe(
      data => this.router.navigateByUrl('/unit-types'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
