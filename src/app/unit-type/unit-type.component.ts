import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { UnitType, UnitTypeService } from '../shared';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DeleteComponentDialog } from '../shared';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'app-unit-type',
  templateUrl: './unit-type.component.html',
})
export class UnitTypeComponent {
  displayedColumns = ['name', 'action'];
  dataSource: UnitTypeDataSource | null;
  dataSubject = new BehaviorSubject<any[]>([]);
  errorMessage: string;
  loading: boolean = false;

  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private unitTypeService: UnitTypeService,
    public dialog: MatDialog
  ) {}

  getunitTypes(): void {
    this.loading = true;
    this.unitTypeService.getAll().subscribe(categories => {
      this.dataSubject.next(categories);
      this.loading = false;
    }, error => (this.errorMessage = <any>error));

    this.dataSource = new UnitTypeDataSource(this.dataSubject, this.paginator);
  }

  ngOnInit() {
    this.getunitTypes();
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  openDialog(unitType) {
    let data = {
      name: unitType.name,
      title: 'Satuan',
    };
    let dialogRef = this.dialog.open(DeleteComponentDialog, {
      data: data,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteUnitType(unitType.id);
      }
    });
  }

  deleteUnitType(unitTypeId) {
    this.loading = true;
    this.unitTypeService.delete(unitTypeId).subscribe(
      data => this.getunitTypes(),
      err => {
        alert(err);
      }
    );
  }
}

export class UnitTypeDataSource extends DataSource<any[]> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: UnitType[] = [];

  constructor(
    private subject: BehaviorSubject<any[]>,
    private _paginator: MatPaginator
  ) {
    super();
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.subject,
      this._paginator.page,
      this._filterChange,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      // filter data by search term
      this.filteredData = this.subject.value
        .slice()
        .filter((item: UnitType) => {
          let searchStr = item.name.toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) != -1;
        });

      // copy data so it doesn't get mutated
      const data: UnitType[] = this.filteredData.slice();

      // Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect(): void {}
}
