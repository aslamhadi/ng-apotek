import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import {
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatInputModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatTableModule,
  MatSelectModule,
} from '@angular/material';

import { AuthComponent } from './auth/auth.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoAuthGuard } from './auth/no-auth-guard.service';
import { NotFoundComponent } from './not-found/not-found.component';

import {
  AuthGuard,
  SharedModule,
  CategoryService,
  SubCategoryService,
  DoctorService,
  EmbalaseService,
  ApiService,
  JwtService,
  UserService,
  UnitTypeService,
  DeleteComponentDialog,
} from './shared';

import {
  CategoryComponent,
  CategoryAddComponent,
  CategoryEditComponent,
} from './category';

import {
  SubCategoryComponent,
  SubCategoryAddComponent,
  SubCategoryEditComponent,
} from './subcategory';

import {
  DoctorComponent,
  DoctorAddComponent,
  DoctorEditComponent,
} from './doctor';

import {
  EmbalaseComponent,
  EmbalaseAddComponent,
  EmbalaseEditComponent,
} from './embalase';

import {
  UnitTypeComponent,
  UnitTypeAddComponent,
  UnitTypeEditComponent,
} from './unit-type';

@NgModule({
  imports: [
    BrowserModule,
    CdkTableModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SharedModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatPaginatorModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSelectModule,
  ],
  declarations: [
    AuthComponent,
    AppComponent,
    CategoryComponent,
    CategoryAddComponent,
    CategoryEditComponent,
    DoctorComponent,
    DoctorAddComponent,
    DoctorEditComponent,
    EmbalaseComponent,
    EmbalaseAddComponent,
    EmbalaseEditComponent,
    SubCategoryComponent,
    SubCategoryAddComponent,
    SubCategoryEditComponent,
    NotFoundComponent,
    UnitTypeComponent,
    UnitTypeAddComponent,
    UnitTypeEditComponent,
    DeleteComponentDialog,
  ],
  providers: [
    ApiService,
    JwtService,
    CategoryService,
    SubCategoryService,
    DoctorService,
    EmbalaseService,
    UserService,
    UnitTypeService,
    NoAuthGuard,
    AuthGuard,
  ],
  bootstrap: [AppComponent],
  entryComponents: [DeleteComponentDialog],
})
export class AppModule {
  constructor() {}
}
