import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  CategoryComponent,
  CategoryAddComponent,
  CategoryEditComponent,
} from './category';

import {
  SubCategoryComponent,
  SubCategoryAddComponent,
  SubCategoryEditComponent,
} from './subcategory';

import {
  DoctorComponent,
  DoctorAddComponent,
  DoctorEditComponent,
} from './doctor';

import {
  EmbalaseComponent,
  EmbalaseAddComponent,
  EmbalaseEditComponent,
} from './embalase';


import {
  UnitTypeComponent,
  UnitTypeAddComponent,
  UnitTypeEditComponent,
} from './unit-type';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { NoAuthGuard } from './auth/no-auth-guard.service';
import { AuthGuard } from './shared';

const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent,
    canActivate: [NoAuthGuard],
  },
  {
    path: 'register',
    component: AuthComponent,
    canActivate: [NoAuthGuard],
  },
  {
    path: 'categories',
    component: CategoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'categories/add',
    component: CategoryAddComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'categories/:id',
    component: CategoryEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'subcategories',
    component: SubCategoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'subcategories/add',
    component: SubCategoryAddComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'subcategories/:id',
    component: SubCategoryEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'doctors',
    component: DoctorComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'doctors/add',
    component: DoctorAddComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'doctors/:id',
    component: DoctorEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'embalases',
    component: EmbalaseComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'embalases/add',
    component: EmbalaseAddComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'embalases/:id',
    component: EmbalaseEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'unit-types',
    component: UnitTypeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'unit-types/add',
    component: UnitTypeAddComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'unit-types/:id',
    component: UnitTypeEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: '',
    redirectTo: '/categories',
    pathMatch: 'full',
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
