import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { Embalase, Errors, EmbalaseService } from '../shared';

@Component({
  selector: 'edit-embalase',
  templateUrl: './embalase.add.component.html',
})
export class EmbalaseEditComponent implements OnInit {
  embalases: Embalase[] = [];
  errorMessage: string;
  embalaseForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  embalase: Embalase;
  embalaseId: string = '';
  pageTitle: string = 'Ubah Dokter';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private embalaseService: EmbalaseService,
    private fb: FormBuilder
  ) {
    this.embalaseForm = this.fb.group({
      name: [null, Validators.required],
      price: [null, Validators.required],
    });
  }

  ngOnInit() {
    this.getEmbalase();
  }

  getEmbalase() {
    this.route.params
      .switchMap(
        (params: Params) => (
          (this.embalaseId = params['id']),
          this.embalaseService.get(params['id'])
        )
      )
      .subscribe((embalase: Embalase) => {
        this.embalase = embalase;
        this.embalaseForm.patchValue({
          name: embalase.name,
          price: embalase.price,
        });
      });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let embalase = this.embalaseForm.value;
    this.embalaseService.update(embalase, this.embalaseId).subscribe(
      data => this.router.navigateByUrl('/embalases'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
