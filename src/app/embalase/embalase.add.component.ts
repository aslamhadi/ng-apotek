import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

import { Embalase, Errors, EmbalaseService } from '../shared';

@Component({
  selector: 'app-embalase',
  templateUrl: './embalase.add.component.html',
})
export class EmbalaseAddComponent {
  embalases: Embalase[] = [];
  errorMessage: string;
  embalaseForm: FormGroup;
  isSubmitting: boolean = false;
  errors: Errors = new Errors();
  title = 'Submit';
  moduleName: string = 'Embalase';
  pageTitle: string = 'Tambah ' + this.moduleName;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private embalaseService: EmbalaseService,
    private fb: FormBuilder
  ) {
    this.embalaseForm = this.fb.group({
      name: [null, Validators.required],
      price: [null, Validators.required],
    });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    let embalase = this.embalaseForm.value;
    this.embalaseService.add(embalase).subscribe(
      data => this.router.navigateByUrl('/embalases'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
